from sqlalchemy import Column, String, DateTime
from dataclasses import dataclass
from datetime import datetime

from sqlalchemy.sql.sqltypes import Integer

from app.configs.database import db


@dataclass
class LeadsModel(db.Model):

    name: str
    email: str
    phone: str
    creation_date: datetime
    last_visit: datetime
    visits: int

    __tablename__ = "leads_cards"

    id = Column(Integer, primary_key=True)
    name = Column(String, nullable=False)
    email = Column(String, unique=True, nullable=False)
    phone = Column(String, unique=True, nullable=False)
    creation_date = Column(DateTime, nullable=True, default=datetime.today)
    last_visit = Column(DateTime, nullable=True, default=datetime.today)
    visits = Column(Integer, nullable=True, default=1)

    def update_number_of_visits(self):
        self.visits = self.visits + 1

    def update_last_visit(self):
        self.last_visit = datetime.today()
