from flask import current_app, jsonify, request
from datetime import datetime
from app.models.leads_model import LeadsModel
from sqlalchemy.exc import IntegrityError, NoResultFound
from re import fullmatch
from app.exc.exceptions import InvalidNumberPhone, NoDataFound


def post_data():
    try:
        data = request.get_json()

        if fullmatch("\(\d{2}\)\d{5}\-\d{4}", data['phone']) is None:
            raise InvalidNumberPhone(
                "Telefone obrigatoriamente no formato (xx)xxxxx-xxxx."
                )

        new_lead = LeadsModel(
            name=data["name"],
            email=data["email"],
            phone=data["phone"],
            creation_date=datetime.today(),
            last_visit=datetime.today(),
        )

        session = current_app.db.session
        session.add(new_lead)
        session.commit()

        return jsonify(new_lead), 201
    except IntegrityError as e:
        return {"Error": str(e.orig).split("\n")[0]}, 400
    except InvalidNumberPhone as e:
        return {"Error": str(e)}, 400


def get_data():
    try:
        data = LeadsModel.query.all()
        if data == []:
            raise NoDataFound
        return jsonify(data)
    except NoDataFound:
        return jsonify({"message": "Nenhum dado encontrado."}), 400


def patch_data():
    data = request.get_json()

    try:
        query = LeadsModel.query.filter_by(email=data['email']).first()
        LeadsModel.update_number_of_visits(query)
        LeadsModel.update_last_visit(query)

        session = current_app.db.session
        session.commit()

        return '', 204
    except KeyError:
        return {'message': 'chave invalida'}, 404
    except AttributeError:
        return {'message': 'Nenhum dado encontrado.'}, 404


def delete_data():
    try:
        data = request.get_json()
        query = LeadsModel.query.filter_by(
                                        email=data['email']).first()

        session = current_app.db.session
        session.delete(query)
        session.commit()

        return jsonify(query), 204

    except KeyError:
        return {'message': 'chave invalida'}, 404
    except NoResultFound:
        return {'message': 'Nenhuma linha foi encontrada quando\
                 uma era necessária'}, 404
