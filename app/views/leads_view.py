from flask import Blueprint

from app.services.leads_service import post_data, get_data, patch_data,\
                                        delete_data


bp = Blueprint("leads", __name__, url_prefix="/lead")


@bp.post('')
def create():
    return post_data()


@bp.get('')
def get_all():
    return get_data()


@bp.patch('')
def update_user():
    return patch_data()


@bp.delete('')
def delete_user():
    return delete_data()
